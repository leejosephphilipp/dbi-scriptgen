package hk.com.novare.data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Persistent entity class mapped to EFS_SERVICE_FEES table.
 */
@Entity
@Table(schema = "ACQ_USER",
       name = "EFS_SERVICE_FEES")
@IdClass(ServiceFee.class)
public class ServiceFee
        implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CLIENT_ID")
    private String clientId;

    @Id
    @Column(name = "MER_ID")
    private String merId;

    @Id
    @Column(name = "ROUTING")
    private String routing;

    @Column(name = "CARD_TYPE") private String cardType;

    @Column(name = "SMI_SHARE") private Integer smiShare;

    @Column(name = "PERCENTAGE_FEE") private Integer percentageFee;

    @Column(name = "PF_SHARE") private Integer pfShare;

    @Column(name = "PASS_ON_RATE") private Integer passOnRate;

    @Column(name = "OTHER_CHARGES") private Integer otherCharges;

    public String getClientId() {
        return clientId;
    }

    public String getMerId() {
        return merId;
    }

    public String getRouting() {
        return routing;
    }

    public String getCardType() {
        return cardType;
    }

    public Integer getSmiShare() {
        return smiShare;
    }

    public Integer getPercentageFee() {
        return percentageFee;
    }

    public Integer getPfShare() {
        return pfShare;
    }

    public Integer getPassOnRate() {
        return passOnRate;
    }

    public Integer getOtherCharges() {
        return otherCharges;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (
                (cardType == null) ?
                        0 :
                        cardType.hashCode()
        );
        result = prime * result + (
                (clientId == null) ?
                        0 :
                        clientId.hashCode()
        );
        result = prime * result + (
                (merId == null) ?
                        0 :
                        merId.hashCode()
        );
        result = prime * result + (
                (otherCharges == null) ?
                        0 :
                        otherCharges.hashCode()
        );
        result = prime * result + (
                (passOnRate == null) ?
                        0 :
                        passOnRate.hashCode()
        );
        result = prime * result + (
                (percentageFee == null) ?
                        0 :
                        percentageFee.hashCode()
        );
        result = prime * result + (
                (pfShare == null) ?
                        0 :
                        pfShare.hashCode()
        );
        result = prime * result + (
                (routing == null) ?
                        0 :
                        routing.hashCode()
        );
        result = prime * result + (
                (smiShare == null) ?
                        0 :
                        smiShare.hashCode()
        );
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ServiceFee other = (ServiceFee) obj;
        if (cardType == null) {
            if (other.cardType != null) {
                return false;
            }
        }
        else if (!cardType.equals(other.cardType)) {
            return false;
        }
        if (clientId == null) {
            if (other.clientId != null) {
                return false;
            }
        }
        else if (!clientId.equals(other.clientId)) {
            return false;
        }
        if (merId == null) {
            if (other.merId != null) {
                return false;
            }
        }
        else if (!merId.equals(other.merId)) {
            return false;
        }
        if (otherCharges == null) {
            if (other.otherCharges != null) {
                return false;
            }
        }
        else if (!otherCharges.equals(other.otherCharges)) {
            return false;
        }
        if (passOnRate == null) {
            if (other.passOnRate != null) {
                return false;
            }
        }
        else if (!passOnRate.equals(other.passOnRate)) {
            return false;
        }
        if (percentageFee == null) {
            if (other.percentageFee != null) {
                return false;
            }
        }
        else if (!percentageFee.equals(other.percentageFee)) {
            return false;
        }
        if (pfShare == null) {
            if (other.pfShare != null) {
                return false;
            }
        }
        else if (!pfShare.equals(other.pfShare)) {
            return false;
        }
        if (routing == null) {
            if (other.routing != null) {
                return false;
            }
        }
        else if (!routing.equals(other.routing)) {
            return false;
        }
        if (smiShare == null) {
            if (other.smiShare != null) {
                return false;
            }
        }
        else if (!smiShare.equals(other.smiShare)) {
            return false;
        }
        return true;
    }
}
