package hk.com.novare.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistent entity class mapped to EFS_SPEC_BILLER_LIST table.
 */
@Entity
@Table(schema = "ACQ_USER",
       name = "EFS_SPEC_BILLER_LIST")
public class SpecBillerList {
    @Id
    @Column(name = "CLIENT_ID")
    private String clientId;

    @Column(name = "BILLER_SLUG") private String billerSlug;

    @Column(name = "MER_ID") private String merId;

    public String getClientId() {
        return clientId;
    }

    public String getBillerSlug() {
        return billerSlug;
    }

    public String getMerId() {
        return merId;
    }
}