package hk.com.novare.data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Persistent entity class mapped to EFS_BILLER_ICON_URLS table.
 */
@Entity
@Table(schema = "ACQ_USER",
       name = "EFS_BILLER_ICON_URLS")
@IdClass(BillerIconUrl.class)
public class BillerIconUrl
        implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BILLER_SLUG")
    private String billerSlug;

    @Id
    @Column(name = "OS")
    private String os;

    @Id
    @Column(name = "TYPE")
    private String type;

    @Id
    @Column(name = "LINK")
    private String link;

    @Id
    @Column(name = "RESOLUTION")
    private String resolution;

    public String getBillerSlug() {
        return billerSlug;
    }

    public String getOs() {
        return os;
    }

    public String getType() {
        return type;
    }

    public String getLink() {
        return link;
    }

    public String getResolution() {
        return resolution;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (
                (billerSlug == null) ?
                        0 :
                        billerSlug.hashCode()
        );
        result = prime * result + (
                (link == null) ?
                        0 :
                        link.hashCode()
        );
        result = prime * result + (
                (os == null) ?
                        0 :
                        os.hashCode()
        );
        result = prime * result + (
                (resolution == null) ?
                        0 :
                        resolution.hashCode()
        );
        result = prime * result + (
                (type == null) ?
                        0 :
                        type.hashCode()
        );
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BillerIconUrl other = (BillerIconUrl) obj;
        if (billerSlug == null) {
            if (other.billerSlug != null) {
                return false;
            }
        }
        else if (!billerSlug.equals(other.billerSlug)) {
            return false;
        }
        if (link == null) {
            if (other.link != null) {
                return false;
            }
        }
        else if (!link.equals(other.link)) {
            return false;
        }
        if (os == null) {
            if (other.os != null) {
                return false;
            }
        }
        else if (!os.equals(other.os)) {
            return false;
        }
        if (resolution == null) {
            if (other.resolution != null) {
                return false;
            }
        }
        else if (!resolution.equals(other.resolution)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            }
        }
        else if (!type.equals(other.type)) {
            return false;
        }
        return true;
    }
}