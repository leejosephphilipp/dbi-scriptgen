package hk.com.novare.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Persistent entity class mapped to EFS_BILLERS table.
 */
@Entity
@Table(schema = "ACQ_USER",
       name = "EFS_BILLERS")
public class Biller {
    @Id
    @Column(name = "SLUG")
    private String slug;

    @Column(name = "NAME") private String name;

    @Column(name = "CATEGORY_SLUG") private String categorySlug;

    @Column(name = "ICON_URL") private String iconUrl;

    @Column(name = "ACTIVE") private char active;

    @Column(name = "EFS_MER_ID") private String merId;

    @Column(name = "CREATION_DATE") private Date creationDate;

    @Column(name = "UPDATE_DATE") private Date updateDate;

    @Column(name = "CUSTOM_FIELDS") private String customFields;

    @Column(name = "RECEIPT_FIELD_NAME") private String receiptFieldName;

    @Column(name = "VALIDATE_CONFIG") private String validateConfig;

    @Column(name = "POST_CONFIG") private String postConfig;

    @Column(name = "INQUIRE_CONFIG") private String inquireConfig;

    @Column(name = "EXPIRATION_CONFIG") private String expirationConfig;

    @Column(name = "GET_FEE_CONFIG") private String getFeeConfig;

    public String getSlug() {
        return slug;
    }

    public String getName() {
        return name;
    }

    public String getCategorySlug() {
        return categorySlug;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public char getActive() {
        return active;
    }

    public String getMerId() {
        return merId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public String getCustomFields() {
        return customFields;
    }

    public String getReceiptFieldName() {
        return receiptFieldName;
    }

    public String getValidateConfig() {
        return validateConfig;
    }

    public String getPostConfig() {
        return postConfig;
    }

    public String getInquireConfig() {
        return inquireConfig;
    }

    public String getExpirationConfig() {
        return expirationConfig;
    }

    public String getGetFeeConfig() {
        return getFeeConfig;
    }
}