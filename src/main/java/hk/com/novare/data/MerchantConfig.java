package hk.com.novare.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistent entity class mapped to EFS_MERCHANT_CONFIG table.
 */
@Entity
@Table(schema = "ACQ_USER",
       name = "EFS_MERCHANT_CONFIG")
public class MerchantConfig {
    @Id
    @Column(name = "MER_ID")
    private String merId;

    @Column(name = "AGGREGATOR") private String aggregator;

    @Column(name = "NAME") private String name;

    @Column(name = "MCC") private String mcc;

    @Column(name = "ADDRESS_LINE1") private String addressLine1;

    @Column(name = "ADDRESS_CITY") private String city;

    @Column(name = "ADDRESS_STATE") private String state;

    @Column(name = "ADDRESS_POSTAL_CODE") private String postalCode;

    @Column(name = "ADDRESS_ALPHA_COUNTRY_CODE") private String countryCode;

    @Column(name = "BILLER_CODE") private String billerCode;

    @Column(name = "SERVICE_CODE") private String serviceCode;

    @Column(name = "FULL_NAME") private String fullName;

    public String getMerId() {
        return merId;
    }

    public String getAggregator() {
        return aggregator;
    }

    public String getName() {
        return name;
    }

    public String getMcc() {
        return mcc;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public String getFullName() {
        return fullName;
    }
}