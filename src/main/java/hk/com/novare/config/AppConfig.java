package hk.com.novare.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for setting up beans
 */
@Configuration
public class AppConfig {

    private ClientConfiguration clientConfig;
    private AWSCredentialsProviderChain credentialsProvider;
    private AmazonS3 client;
    /**
     * The AWS region to use as configured in application.properties
     */
    @Value("${aws.region}") private String awsRegion;


    /**
     * Initializes the {@link AmazonS3} bean using the
     * AWS configuration specified in application.properties
     *
     * @return
     */
    @Bean
    public AmazonS3 amazonS3() {
        try {
            clientConfig = new ClientConfiguration().withProxyProtocol(Protocol.HTTP);
            clientConfig.setProxyProtocol(Protocol.HTTP);
            credentialsProvider = new DefaultAWSCredentialsProviderChain();
            client = AmazonS3ClientBuilder.standard()
                    .withRegion(awsRegion)
                    .withClientConfiguration(clientConfig)
                    .withCredentials(credentialsProvider)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error loading credentials",
                    e
            );
        }
        return client;
    }
}
