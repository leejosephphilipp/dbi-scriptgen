package hk.com.novare.service;

/**
 * Interface defining insert script generation operations.
 */
public interface ScriptGenService {
    /**
     * Generates the INSERT script for a Biller using the given SLUG.
     *
     * @return The INSERT script
     */
    String generateInsertScript(String billerSlug);
}
