package hk.com.novare.service;

import hk.com.novare.data.*;
import hk.com.novare.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class implementing {@link ScriptGenService}
 */
@Service
public class ScriptGenServiceImpl
        implements ScriptGenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptGenServiceImpl.class);

    @Value("${local.directory}") private String localDirectory;

    private Path dirPath;

    @Autowired BillerRepository billerRepo;

    @Autowired MerchantConfigRepository merConfigRepo;

    @Autowired SpecBillerListRepository specBillerListRepo;

    @Autowired ServiceFeeRepository serviceFeeRepo;

    @Autowired BillerIconUrlRepository billerIconRepo;

    @Override
    public String generateInsertScript(String billerSlug) {
        Biller biller = billerRepo.findById(billerSlug)
                .get();
        MerchantConfig merConfig = merConfigRepo.findById(biller.getMerId())
                .get();
        List<SpecBillerList> billerLists = specBillerListRepo.findByMerId(biller.getMerId());
        List<ServiceFee> serviceFees = serviceFeeRepo.findByMerId(biller.getMerId());
        List<BillerIconUrl> iconUrls = billerIconRepo.findByBillerSlug(billerSlug);

        String filename = billerSlug + ".sql";

        try {
            dirPath = checkDirectories(localDirectory);
            String fileDir = dirPath + "/" + filename;
            try (PrintWriter writer = new PrintWriter(new File(fileDir))) {
                writer.printf("%s",
                              stripNulls(toInsertScript(biller)));
                writer.print("\r\n");
                writer.printf("%s",
                              stripNulls(toInsertScript(merConfig)));
                writer.print("\r\n");
                writer.printf("%s",
                              stripNulls(toInsertScriptSpecBillerList(billerLists)));
                writer.print("\r\n");
                writer.printf("%s",
                              stripNulls(toInsertScriptServiceFees(serviceFees)));
                writer.print("\r\n");
                writer.printf("%s",
                              stripNulls(toInsertScriptIconUrls(iconUrls)));
            }
            LOGGER.info(
                    "The file has been successfully created {}",
                    filename
            );
            return fileDir;
        } catch (Exception e) {
            e.printStackTrace();
            return "FAILED";
        }
    }

    private String toInsertScript(Biller biller) {
        return "INSERT INTO ACQ_USERS.EFS_BILLERS (SLUG,NAME,CATEGORY_SLUG,ICON_URL,"
                + "ACTIVE,EFS_MER_ID,CREATION_DATE,UPDATE_DATE,CUSTOM_FIELDS,"
                + "RECEIPT_FIELD_NAME,VALIDATE_CONFIG,POST_CONFIG,"
                + "INQUIRE_CONFIG,EXPIRATION_CONFIG,GET_FEE_CONFIG) " + "VALUES ('" + biller.getSlug() + "','"
                + biller.getName() + "','" + biller.getCategorySlug() + "','" + biller.getIconUrl() + "','"
                + biller.getActive() + "','" + biller.getMerId() + "'," + "to_date('"
                + formatDate(biller.getCreationDate()) + "','MM/DD/RRRR')," + "to_date('"
                + formatDate(biller.getUpdateDate()) + "','MM/DD/RRRR'),'" + biller.getCustomFields() + "','"
                + biller.getReceiptFieldName() + "','" + biller.getValidateConfig() + "','" + biller.getPostConfig()
                + "','" + biller.getInquireConfig() + "','" + biller.getExpirationConfig() + "','"
                + biller.getGetFeeConfig() + "');";
    }

    private String toInsertScript(MerchantConfig merConfig) {
        return "INSERT INTO ACQ_USERS.EFS_MERCHANT_CONFIG (MER_ID,AGGREGATOR,NAME,"
                + "MCC,ADDRESS_LINE1,ADDRESS_CITY,ADDRESS_STATE," + "ADDRESS_POSTAL_CODE,ADDRESS_ALPHA_COUNTRY_CODE,"
                + "BILLER_CODE,SERVICE_CODE,FULL_NAME) " + "VALUES ('" + merConfig.getMerId() + "','"
                + merConfig.getAggregator() + "','" + merConfig.getName() + "','" + merConfig.getMcc() + "','"
                + merConfig.getAddressLine1() + "','" + merConfig.getCity() + "','" + merConfig.getState() + "','"
                + merConfig.getPostalCode() + "','" + merConfig.getCountryCode() + "','" + merConfig.getBillerCode()
                + "','" + merConfig.getServiceCode() + "','" + merConfig.getFullName() + "');";
    }

    private String toInsertScriptSpecBillerList(List<SpecBillerList> billerLists) {
        return billerLists.stream()
                .map(this::toInsertScript)
                .collect(Collectors.joining("\r\n"));
    }

    private String toInsertScript(SpecBillerList billerList) {
        return "INSERT INTO ACQ_USERS.EFS_SPEC_BILLER_LIST (CLIENT_ID,BILLER_SLUG,MER_ID) " + "VALUES ('"
                + billerList.getClientId() + "','" + billerList.getBillerSlug() + "','" + billerList.getMerId() + "');";
    }

    private String toInsertScriptServiceFees(List<ServiceFee> serviceFees) {
        return serviceFees.stream()
                .map(this::toInsertScript)
                .collect(Collectors.joining("\r\n"));
    }

    private String toInsertScript(ServiceFee serviceFee) {
        return "INSERT INTO ACQ_USERS.EFS_SERVICE_FEES (CLIENT_ID,MER_ID,"
                + "ROUTING,CARD_TYPE,SMI_SHARE,PERCENTAGE_FEE,PF_SHARE,PASS_ON_RATE,OTHER_CHARGES) " + "VALUES ('"
                + serviceFee.getClientId() + "','" + serviceFee.getMerId() + "','" + serviceFee.getRouting() + "','"
                + serviceFee.getCardType() + "'," + serviceFee.getSmiShare() + "," + serviceFee.getPercentageFee() + ","
                + serviceFee.getPfShare() + "," + serviceFee.getPassOnRate() + "," + serviceFee.getOtherCharges()
                + ");";
    }

    private String toInsertScriptIconUrls(List<BillerIconUrl> iconUrls) {
        return iconUrls.stream()
                .map(this::toInsertScript)
                .collect(Collectors.joining("\r\n"));
    }

    private String toInsertScript(BillerIconUrl iconUrl) {
        return "INSERT INTO ACQ_USERS.EFS_BILLER_ICON_URLS (BILLER_SLUG,OS" + ",TYPE,LINK,RESOLUTION) " + "VALUES ('"
                + iconUrl.getBillerSlug() + "','" + iconUrl.getOs() + "','" + iconUrl.getType() + "','"
                + iconUrl.getLink() + "','" + iconUrl.getResolution() + "');";
    }

    private String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        return format.format(date);
    }

    private String stripNulls(String input) {
        return input.replaceAll(
                "'null'",
                "null"
        );
    }


    private Path checkDirectories(String directory) throws Exception {
        Path initialPath = Paths.get(directory);
        if (!Files.exists(initialPath)) {
            return Files.createDirectories(initialPath);
        }
        else {
            return initialPath;
        }
    }
}