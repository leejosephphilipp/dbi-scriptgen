package hk.com.novare.service;

import com.amazonaws.services.s3.AmazonS3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * Class implementing {@link S3Service} methods
 */
@Service
public class S3ServiceImpl
        implements S3Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3ServiceImpl.class);

    @Autowired private AmazonS3 s3Client;

    @Value("${aws.s3.bucketName}") private String bucketName;

    @Override
    public void upload(String directory) {
        File file = new File(directory);
        LOGGER.info("Uploading sql file to s3...");
        try {
            s3Client.putObject(
                    bucketName,
                    file.getName(),
                    file
            );
            LOGGER.info("File successfully uploaded");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Failed to upload the file");
        }
    }
}
