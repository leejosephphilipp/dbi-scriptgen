package hk.com.novare.service;

/**
 * Interface defining S3 operations
 */
public interface S3Service {
    /**
     * Uploads a sql file string to S3 in the configured bucket.
     *
     * @param directory The path where the file is
     */
    void upload(String directory);
}
