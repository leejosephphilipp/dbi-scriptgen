package hk.com.novare.repository;

import hk.com.novare.data.SpecBillerList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Interface defining data operations for {@link SpecBillerList}.
 */
@Repository
public interface SpecBillerListRepository
        extends CrudRepository<SpecBillerList, String> {
    /**
     * Retrieves all spec biller lists using the given Merchant ID.
     *
     * @param merId The merchant ID to use.
     * @return all biller lists with the given Merchant ID.
     */
    List<SpecBillerList> findByMerId(String merId);
}