package hk.com.novare.repository;

import hk.com.novare.data.ServiceFee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Interface defining data operations for {@link ServiceFee}.
 */
public interface ServiceFeeRepository
        extends CrudRepository<ServiceFee, String> {
    /**
     * Retrieves all service fees using the given Merchant ID.
     *
     * @param merId The  Merchant ID for the service fees.
     * @return All service fees using the given Merchant ID.
     */
    List<ServiceFee> findByMerId(String merId);
}