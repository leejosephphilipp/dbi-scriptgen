package hk.com.novare.repository;

import hk.com.novare.data.BillerIconUrl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface defining data operations for {@link BillerIconUrl}.
 */
@Repository
public interface BillerIconUrlRepository
        extends CrudRepository<BillerIconUrl, String> {
    /**
     * Retrieves all biller icon URLs using the given SLUG.
     *
     * @param billerSlug The SLUG for the biller
     * @return All biller icon URLs using the given SLUG.
     */
    List<BillerIconUrl> findByBillerSlug(String billerSlug);
}
