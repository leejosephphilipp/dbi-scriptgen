package hk.com.novare.repository;

import hk.com.novare.data.Biller;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface defining data operations for {@link Biller}.
 */
@Repository
public interface BillerRepository
        extends CrudRepository<Biller, String> {

}
