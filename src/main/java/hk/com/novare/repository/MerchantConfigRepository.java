package hk.com.novare.repository;

import hk.com.novare.data.MerchantConfig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Interface defining data operations for {@link MerchantConfig}.
 */
@Repository
public interface MerchantConfigRepository
        extends CrudRepository<MerchantConfig, String> {

}