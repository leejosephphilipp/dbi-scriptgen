package hk.com.novare;

import hk.com.novare.service.S3Service;
import hk.com.novare.service.ScriptGenService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Main Spring Boot application class used to bootstrap and run the application
 */
@SpringBootApplication
public class DbiScriptGenApplication {
    public static void main(String[] args) {
        ApplicationContext appCtx = SpringApplication.run(
                DbiScriptGenApplication.class,
                args
        );
        ScriptGenService scriptGenService = appCtx.getBean(ScriptGenService.class);
        S3Service s3Service = appCtx.getBean(S3Service.class);
        s3Service.upload(scriptGenService.generateInsertScript(args[0]));
    }
}